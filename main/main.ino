#include <SoftwareSerial.h>

const uint8_t rxPin = 3;
const uint8_t txPin = 4;

const uint8_t buttonPin = 2;
const uint8_t ledPin =  13;

SoftwareSerial bluetoothSerial(txPin, rxPin);

void pin2ISR()
{
    digitalWrite(13, !digitalRead(13));    // Toggle LED on pin 13
    bluetoothSerial.println("Hello from interrupt!");
}

void setup() {
  Serial.begin(9600);
  bluetoothSerial.begin(9600);

  pinMode(ledPin, OUTPUT);

  pinMode(buttonPin, INPUT);
  digitalWrite(buttonPin, HIGH);    // Enable pullup resistor

  attachInterrupt(0, pin2ISR, FALLING);
}

void loop() {

}
